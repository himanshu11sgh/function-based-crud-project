from django.shortcuts import render
from django.http import HttpResponseRedirect

from .models import User
from .forms import UserForm

# Create your views here.


def add_data(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()

    form = UserForm()

    users = User.objects.all()
    context = {
        'form': form,
        'users': users
    }
    return render(request, 'core/add_del.html', context)


def update_data(request, id):
    if request.method == 'POST':
        user = User.objects.get(id=id)
        form = UserForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        user = User.objects.get(id=id)
        form = UserForm(instance=user)

    context = {
        'form': form
    }
    return render(request, 'core/update.html', context)
        

def del_data(request, id):
    user = User.objects.get(id=id)
    user.delete()
    return HttpResponseRedirect('/')
from django.urls import path

from .views import *


urlpatterns = [ 
    path('', add_data, name='add'),
    path('delete/<int:id>/', del_data, name='del'),
    path('update/<int:id>/', update_data, name='update'),
]